import sys, os, re
import requests
import re
import difflib, urllib

from bs4 import BeautifulSoup
from urllib.parse import quote

mpv_socket = sys.argv[1]
sub_file = sys.argv[2]

zoro_regex = r"https:\/\/zoro\.to\/watch?\/([\w-]+)-(\d+)\?ep=(\d+)"
url = sys.argv[3]

id = re.findall(zoro_regex, url)
series_id = id[0][0]+'-'+id[0][1]
id = series_id+'$episode$'+id[0][2]+"$"


ZORO = 'https://zoro.to/'

api_url = 'https://api.consumet.org/anime/zoro/watch'
params = {"episodeId": id}


res = requests.get(
    api_url,
    params=params
).json()

stream_url = res["sources"][0]["url"]
print(stream_url)

api_url = f'https://api.consumet.org/anime/zoro/info'
params = {"id": series_id}


res = requests.get(
    api_url,
    params=params
).json()


anime_title = res["title"]
for result in res["episodes"]:
    if result["url"] == url:
        episode_number = str(result["number"])
        episode_title = result["title"]

from AnilistPython import Anilist
anilist = Anilist()

def get_list(url):
        response = requests.get(url)
        soup = BeautifulSoup(response.content, 'html.parser')
        strong_list = soup.find_all('strong')
        anime_list = []
        for anime in strong_list:
                anime_list.append(anime.text.strip())
        return anime_list

url = 'https://kitsunekko.net/dirlist.php?dir=subtitles%2Fjapanese%2F'

anime_list = get_list(url)
anime_list_lower = [x.lower() for x in anime_list]

try:
    anime_title_romaji =  anilist.get_anime(anime_title)["name_romaji"]
except:
    anime_title_romaji =  anilist.get_anime(anime_title.split()[0])["name_romaji"]

matches = difflib.get_close_matches(anime_title_romaji, anime_list)
if not len(matches):
    matches = difflib.get_close_matches(anime_title_romaji.lower(), anime_list_lower)
    matches = [anime_list[anime_list_lower.index(x)] for x in matches]
if not len(matches):
    matches = difflib.get_close_matches(anime_title, anime_list)
if not len(matches):
    matches = difflib.get_close_matches(anime_title.lower(), anime_list_lower)
    matches = [anime_list[anime_list_lower.index(x)] for x in matches]

best_match = matches[0]

url += best_match
ep_list = get_list(url)
compressed = ('zip', '7z', 'rar')


files = []
priority_files = []
compressed_files = []
#print(ep_list)
for sub in ep_list:
    if sub.endswith(compressed):
        compressed_files.append(sub)
    else:
        numbers = re.findall(r'\d+', sub)
        if len(numbers):
            for num in numbers:
                if int(num) == int(episode_number):
                    priority_files.append(sub)
                else:
                    files.append(sub)
                    



url2 = 'https://kitsunekko.net/subtitles/japanese'

import os.path

if priority_files:
    for file in priority_files:
        best_file = quote(file.encode("utf-8"))

        full_path = "/tmp/"+file

        if os.path.isfile(full_path):
            print(full_path)
        else:
            download_url = '%s/%s/%s' % (url2, quote(best_match), best_file)
            ##print(download_url)
            try:
                urllib.request.urlretrieve(download_url, full_path)
                print(full_path)
            except:
                pass
else:
    best_file = quote(compressed_files[0].encode("utf-8"))

    full_path = "/tmp/" + compressed_files[0]

    if os.path.isfile(full_path):
        import shutil

        shutil.unpack_archive(full_path, '/tmp/')

        os.chdir("/tmp/")
        import glob, os

        for file in glob.glob("*.srt"):
            if episode_number in file:
                print("/tmp/" + file)
            else:
                os.remove("/tmp/"+file)
        for file in glob.glob("*.ass"):
            if episode_number in file:
                print("/tmp/" + file)
            else:
                os.remove("/tmp/"+file)
    else:
        download_url = '%s/%s/%s' % (url2, quote(best_match), best_file)
        ##print(download_url)
        try:
            urllib.request.urlretrieve(download_url, full_path)

            import shutil

            shutil.unpack_archive(full_path, '/tmp/')

            import glob, os

            os.chdir("/tmp/")
            for file in glob.glob("*.srt"):
                if episode_number in file:
                    print("/tmp/"+file)
                else:
                    os.remove("/tmp/" + file)
            for file in glob.glob("*.ass"):
                if episode_number in file:
                    print("/tmp/"+file)
                else:
                    os.remove("/tmp/" + file)
        except:
            pass
