local utils = require('mp.utils')

sub_file = '/tmp/mpv_sub'
mpv_socket = '/tmp/mpv_socket'
zoro_tmp = '/tmp/zoro-tmp.txt'

-- if you're not using a virtual environment get rid of venv_command
venv_command = 'export PYENV_ROOT="$HOME/.pyenv" && command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH" && eval "$(pyenv init -)" && pyenv shell memento && '
start_command = venv_command .. 'python3 "%s" "%s" "%s" "%s"'
-- if you're using mpv do the following
--pyname = '~/.config/mpv/scripts/zoro-python/zoro.py'
pyname = '~/Library/Preferences/memento/scripts/zoro-python/zoro.py'

function create_socket(id)
  -- credit: https://github.com/HasanAbbadi/jaSubs/
  if running == true then
    delete_socket()
    return
  end

  running = true
  
  mp.msg.warn('Starting socket...')
  mp.register_event("end-file", delete_socket)
  rnbr = math.random(11111111, 99999999)

  mpv_socket_2 = mpv_socket .. '_' .. rnbr
  sub_file_2 =  sub_file .. '_' .. rnbr
 
  -- setting up socket to control mpv
  mp.set_property("input-ipc-server", mpv_socket_2)
  -- without visible subs won't work
  sbv = mp.get_property("sub-visibility")
  mp.set_property("sub-visibility", "yes")
  mp.set_property("sub-ass-override", "force")

  sub_color1 = mp.get_property("sub-color", "1/1/1/1")
  sub_color2 = mp.get_property("sub-border-color", "0/0/0/1")
  sub_color3 = mp.get_property("sub-shadow-color", "0/0/0/1")
  mp.set_property("sub-color", "0/0/0/0")
  mp.set_property("sub-border-color", "0/0/0/0")
  mp.set_property("sub-shadow-color", "0/0/0/0")


  local files = {}
  
  start_command_2 = start_command:format(pyname:gsub('~', os.getenv('HOME')), mpv_socket_2, sub_file_2, id)
  os.execute(start_command_2..' > '..zoro_tmp)-- .. ' &')
  
  local f = io.open(zoro_tmp)
  if not f then return file_lines end
  local k = 1
  for line in f:lines() do
    files[k] = line
    if k ~= 1 then
      mp.commandv('sub-add', 'file://'..line)
    end
    k = k + 1
  end
  f:close()
  
  mp.set_property("stream-open-filename", files[1])

  mp.observe_property("sub-text", "string", s2)
end;

function s2(name, value)
  if type(value) == "string" then
    file = io.open(sub_file_2, "w")
    file:write(value)
    file:close()
  end
end;

function delete_socket()
  running = false
  hidden = false
  mp.msg.warn('Quitting socket...')

  mp.set_property("sub-visibility", sbv)
  mp.set_property("sub-color", sub_color1)
  mp.set_property("sub-border-color", sub_color2)
  --~ mp.set_property("sub-shadow-color", sub_color3)

  os.execute('pkill -f "' .. mpv_socket_2 .. '" &')
  os.execute('(sleep 3 && rm "' .. mpv_socket_2 .. '") &')
  os.execute('(sleep 3 && rm "' .. sub_file_2 .. '") &')
  os.execute('(sleep 3 && rm "' .. zoro_tmp .. '") &')

  local files = {}
  local f = io.open(zoro_tmp)  
  local k = 1
  for line in f:lines() do
    files[k] = line
    if k ~= 1  then
      os.execute('(sleep 3 && rm "' .. line .. '") &')
    end
    k = k + 1
  end
  f:close()
end

function zoro_detector()
  mp.msg.warn('Zoro Hook Loaded')
  mp.command('show-text "Zoro Hook loaded"')

  filename = mp.get_property_native('stream-open-filename')
  id = filename.match(filename, "zoro%.to/watch/(.+)")
  if(id)
  then
    mp.msg.info("Zoro id detected: " .. id)
    create_socket(filename)
  else
    mp.msg.info("Zoro not detected")
    return
  end

end;
mp.add_hook('on_load', 50, zoro_detector)
