# MPV Script to stream zoro videos

## Usage
1. Install requirements
2. Configure `main.lua` with your python file path/mpv script path
3. Enjoy! :)

## Credit
* https://github.com/consumet/api.consumet.org
